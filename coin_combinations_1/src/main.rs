use std::collections::HashMap;

fn main() {
    let mut buffer = String::new();
    std::io::stdin()
        .read_line(&mut buffer)
        .expect("Could not read input");
    let both: Vec<u32> = buffer
        .trim()
        .split_whitespace()
        .map(|e| e.parse().expect("Could not parse"))
        .collect();
    let x: u32 = both[1];

    buffer.clear();
    std::io::stdin()
        .read_line(&mut buffer)
        .expect("Could not read input");
    let coins: Vec<u32> = buffer
        .trim()
        .split_whitespace()
        .map(|e| e.parse().expect("Could not parse"))
        .collect();

    let results: HashMap<u32, u128>;

    
    
    println!(
        "{}",
        solve(x, &coins, &mut HashMap::new()) % (10u128.pow(9) + 7)
    );
}

fn solve(target_sum: u32, coins: &Vec<u32>, solved: &mut HashMap<u32, u128>) -> u128 {
    for coin in coins {
        if coin < &target_sum {
            if !solved.contains_key(&(target_sum - coin)) {
                solved.insert(target_sum - coin, solve(target_sum - coin, coins, solved));
            }
    }
    return *solved.get(target_sum).unwrap();
}
